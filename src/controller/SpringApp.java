package controller;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mkjuegos.model.Coach;

public class SpringApp {
	
	public static void main(String[] args) {
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		Coach bsCoach = context.getBean("bsCoach",Coach.class);
		
		Coach runCoach = context.getBean("runCoach",Coach.class);
		
		
		System.out.println(bsCoach.getWorkout());
		System.out.println(runCoach.getWorkout());
		
		context.close();
	}
}
